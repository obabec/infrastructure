---
default:
  image: quay.io/testing-farm/python-ci-image:2023-06-01-ec6b0f31

variables:
  # GitLab CI runs in world writable directory, we need to explicitely set ANSIBLE_CONFIG
  # https://docs.ansible.com/ansible/devel/reference_appendices/config.html#cfg-in-world-writable-dir
  ANSIBLE_CONFIG: ansible.cfg

workflow:
  rules:
    # for merge requests
    - if: $CI_MERGE_REQUEST_ID
    # for the tip of the default branch
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # for schedules
    - if: $CI_PIPELINE_SOURCE == "schedule"

.rules:
  not_default_branch:
    # do not run for default branch
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never

  not_schedule:
    # do not run for schedules
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never

  only_default_branch:
    # run only for default branch
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      when: never

  only_schedule:
    # run only for schedule
    - if: $CI_PIPELINE_SOURCE != "schedule"
      when: never

  gitlab_ci:
    - changes:
        - .gitlab-ci.yml

  secrets:
    - changes:
        - secrets/credentials.yaml

  terraform_infra:
    - !reference [.rules, secrets]
    - !reference [.rules, gitlab_ci]
    - changes:
        - terraform/environments/common/**/*
        - terraform/modules/**/*
        - terraform/*.tf
        - terraform/*.tftpl
        - .envrc

  terraform_dev:
    - !reference [.rules, terraform_infra]
    - changes:
        - Makefile
        - poetry.lock
        - terraform/environments/dev/**/*

  terraform_staging:
    - !reference [.rules, terraform_infra]
    - changes:
        - terraform/environments/staging/**/*

  terraform_production:
    - !reference [.rules, terraform_infra]
    - changes:
        - terraform/environments/production/**/*

  public_tests:
    - changes:
        - tests/worker/public/**/*

  guest_setup_tests:
    - changes:
        - tests/worker/guest-setup/**/*

direnv:
  stage: test
  before_script:
    - dnf -y install direnv libffi libffi-devel
    - echo -n "$VAULT_PASSWORD" > .vault_pass
  script:
    - direnv allow
    - eval "$(direnv export bash)"
    - make generate-environment-variables

.direnv:
  before_script:
    - dnf -y install direnv libffi libffi-devel podman
    - echo -n "$VAULT_PASSWORD" > .vault_pass
    - direnv allow
    - eval "$(direnv export bash)"
    # WORKAROUND: gitlab runner is hosted on the same network as staging/production
    #             what is breaking DNS resolving, as AWS DNS resolves k8s API locally
    - printf "nameserver 1.1.1.1\nnameserver 8.8.8.8\nnameserver 8.8.4.4\n" > /etc/resolv.conf
  script:
    - eval "$(direnv export bash)"

# Workaround for after_script timing-out
.script_with_cleanup:
  script:
    - /bin/bash -c "${SCRIPT}" && EXIT_CODE=$? || EXIT_CODE=$?
    - /bin/bash -c "${AFTER_SCRIPT}"
    - "[ ${EXIT_CODE} = 0 ]"

# Archive pytest-gluetool artifacts
.archive_tests:
  artifacts:
    expose_as: report
    paths:
      - report.html
      - assets/
    when: always

ansible-vault:
  extends: .direnv
  stage: test
  script:
    - !reference [.direnv, script]
    - dnf -y install ansible-lint
    - >
      ansible-vault view secrets/credentials.yaml | yamllint -d "rules: {line-length: {level: warning}}" -
  rules:
    - !reference [.rules, not_default_branch]
    - !reference [.rules, not_schedule]
    - !reference [.rules, secrets]

pre-commit:
  extends: .direnv
  stage: test
  script:
    - !reference [.direnv, script]
    - PRE_COMMIT_HOME=$CI_PROJECT_DIR/.cache/pre-commit pre-commit run --all-files
  rules:
    - !reference [.rules, not_default_branch]
    - !reference [.rules, not_schedule]
    - when: always

test/dev:
  extends: [.direnv, .archive_tests]
  stage: test
  script:
    # TODO: tmt run -e cluster_name=testing-farm-gitlab-ci-$CI_COMMIT_REF_NAME plan --name /terraform/environments/dev
    - !reference [.direnv, script]
    - !reference [.script_with_cleanup, script]
  variables:
    TF_VAR_cluster_name: testing-farm-gitlab-ci-$CI_JOB_ID
    SCRIPT_DIR: terraform/environments/dev
    PYTEST_OPTIONS: -k 'not container'
    SCRIPT: >-
      make init-dev &&
      make apply-dev &&
      make test-worker-public
    AFTER_SCRIPT: make destroy-dev > /dev/null
  rules:
    - !reference [.rules, not_default_branch]
    - !reference [.rules, not_schedule]
    - !reference [.rules, terraform_dev]
    - !reference [.rules, public_tests]

test/staging:
  extends: .direnv
  stage: test
  script:
    - !reference [.direnv, script]
    - !reference [.script_with_cleanup, script]
  variables:
    SCRIPT_DIR: terraform/environments/staging
    SCRIPT: >-
      terraform -chdir=$SCRIPT_DIR init &&
      terraform -chdir=$SCRIPT_DIR plan -lock-timeout=5m
  rules:
    - !reference [.rules, not_default_branch]
    - !reference [.rules, not_schedule]
    - !reference [.rules, terraform_staging]

test/production:
  extends: .direnv
  stage: test
  script:
    - !reference [.direnv, script]
    - !reference [.script_with_cleanup, script]
  variables:
    SCRIPT_DIR: terraform/environments/production
    SCRIPT: >-
      terraform -chdir=$SCRIPT_DIR init &&
      terraform -chdir=$SCRIPT_DIR plan -lock-timeout=5m

  rules:
    - !reference [.rules, not_default_branch]
    - !reference [.rules, not_schedule]
    - !reference [.rules, terraform_production]

test/guest-setup:
  extends: .direnv
  stage: test
  script:
    # TODO: tmt run -e cluster_name=testing-farm-gitlab-ci-$CI_COMMIT_REF_NAME plan --name /terraform/environments/dev
    - !reference [.direnv, script]
    - !reference [.script_with_cleanup, script]
  variables:
    TF_VAR_cluster_name: testing-farm-gitlab-ci-$CI_JOB_ID
    SCRIPT_DIR: terraform/environments/dev
    PYTEST_OPTIONS: -k 'not container'
    SCRIPT: >-
      make init-dev &&
      make apply-dev &&
      make test-guest-setup
    AFTER_SCRIPT: make destroy-dev > /dev/null
  rules:
    - !reference [.rules, not_default_branch]
    - !reference [.rules, terraform_dev]
    - !reference [.rules, not_schedule]
    - !reference [.rules, guest_setup_tests]

redeploy/staging:
  extends: .direnv
  stage: deploy
  script:
    - !reference [.direnv, script]
    - !reference [.script_with_cleanup, script]
  variables:
    SCRIPT_DIR: terraform/environments/staging
    SCRIPT: >-
      terraform -chdir=$SCRIPT_DIR init &&
      terraform -chdir=$SCRIPT_DIR destroy -auto-approve &&
      terraform -chdir=$SCRIPT_DIR apply -auto-approve
  rules:
    - !reference [.rules, only_default_branch]
    - !reference [.rules, only_schedule]
    - !reference [.rules, terraform_staging]

deploy/staging:
  extends: .direnv
  stage: deploy
  script:
    - !reference [.direnv, script]
    - !reference [.script_with_cleanup, script]
  variables:
    SCRIPT_DIR: terraform/environments/staging
    SCRIPT: >-
      terraform -chdir=$SCRIPT_DIR init &&
      terraform -chdir=$SCRIPT_DIR apply -auto-approve
  rules:
    - !reference [.rules, only_default_branch]
    - !reference [.rules, not_schedule]
    - !reference [.rules, terraform_staging]

deploy/production:
  extends: .direnv
  stage: deploy
  needs:
    - job: deploy/staging
      optional: true
  script:
    - !reference [.direnv, script]
    - !reference [.script_with_cleanup, script]
  variables:
    SCRIPT_DIR: terraform/environments/production
    SCRIPT: >-
      terraform -chdir=$SCRIPT_DIR init &&
      terraform -chdir=$SCRIPT_DIR apply -auto-approve
  rules:
    - !reference [.rules, only_default_branch]
    - !reference [.rules, not_schedule]
    - !reference [.rules, terraform_production]
