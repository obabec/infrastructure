---

#
# Add tag repository with low priority, for installing additional dependencies of tested artifacts.
#
# The tag repository is updated by Brew/Koji. It contains all packages that got into a specific tag.
# The repository is sometimes also referred to as buildroot or brewroot repository inside Red Hat.
#
# Examples for RHEL:
# * rhel-8.1.0-build contains all builds tagged into rhel-8.1.0-candidate
# * rhel-8.1.0-z-build contains all builds tagged into rhel-8.1.0-z-candidate
# * rhel-7.7-build contains all builds tagged into rhel-7.7-candidate
# * rhel-9.0.0-beta-build contains all builds tagged into rhel-9.0.0-beta-candidate
#
# Note that *-build tags inherit from *-candidate tags
#
# The play gracefully skips adding a tag repository which is not reachable.
#
# Testing Farm needs to deduce the tag repository according to the tested image name.
# For now we try first adding the Z-Stream tag repository and fallback to non Z-Stream.
#
# Issues:
# * TFT-1256 (Z-stream support)
#

- block:

    #
    # The tag repository name can be transformed from IMAGE_NAME variable.
    #
    - name: Transform build target to a tag repository name (RHEL)
      set_fact:
        tag_repository_name_mainline: "{{ IMAGE_NAME | regex_replace('(?i)^.*rhel-(\\d+\\.\\d+(?:\\.\\d+)?(?:-alpha|-beta)?).*$', 'rhel-\\1-build') | lower }}"
        tag_repository_name_z_stream: "{{ IMAGE_NAME | regex_replace('(?i)^.*rhel-(\\d+\\.\\d+(?:\\.\\d+)?(?:-alpha|-beta)?).*$',\
                                       'rhel-\\1-z-build') | lower }}"

    - name: Create tag repository base url (RHEL)
      set_fact:
        tag_repository_url_mainline: "http://download.devel.redhat.com/brewroot/repos/{{ tag_repository_name_mainline }}/latest/{{ ansible_architecture }}/"
        tag_repository_url_z_stream: "http://download.devel.redhat.com/brewroot/repos/{{ tag_repository_name_z_stream }}/latest/{{ ansible_architecture }}/"

    #
    # Set the priority to lowest, this will make sure latest updates are not pulled in instead of default repository
    #
    - name: Set tag repository priority
      set_fact:
        tag_repository_priority: 999

    #
    # Check if tag repository exists.
    #
    # Note: We ignore the fact the repository does not exist and try to continue anyway. Note that not all builds
    #       we encounter might have tag repository available.
    #
    - block:
        - name: "Check if tag repository {{ tag_repository_url_z_stream }} exists"
          # NOTE: order is important, we prefer Z-Stream tag repository if exists
          command: >
            bash -c 'for URL in {{ tag_repository_url_z_stream }} {{ tag_repository_url_mainline }}; do
              curl --head $URL 2>/dev/null | grep -q "200" && echo $URL && break;
            done'
          register: output

        - name: Set tag_repository_url fact
          set_fact:
            tag_repository_url: "{{ output.stdout }}"
          when: output is defined and output.stdout != ""

    #
    # Add repository only if the repository is reachable
    #
    # For < RHEL8 we need to install yum-plugin-priorities
    #
    - block:
        - name: Make sure yum-plugin-priorities is installed on < RHEL8
          package:
            name: yum-plugin-priorities
            state: present
          retries: "{{ pkg_retry_count }}"
          delay: "{{ pkg_retry_delay }}"
          until: result_package is succeeded
          register: result_package

          when:
            - ansible_distribution == "RedHat"
            - ansible_distribution_major_version|int < 8

        - name: Add tag repository
          template:
            src: "../templates/tag.repo.j2"
            dest: /etc/yum.repos.d/tag-repository.repo

      when: tag_repository_url is defined

    #
    # block: end
    #

    #
    # Be verbose about the fact the repository is unreachable
    #
    - debug:
        msg: "Skipping adding tag repository, because no available tag repository was found"
      when: tag_repository_url is not defined

  #
  # Run block if BUILD_TARGET and PRIMARY_TASK variables are available.
  # Also, we do not support tag repository for all known task namespaces.
  #

  when:
    - IMAGE_NAME is defined
    - ansible_distribution == "RedHat"
    - 'not IMAGE_NAME|lower|regex_search("^auto-")'

  #
  # block: end
  #

#
# Be verbose about the fact we skipped this play
#
- debug:
    msg: |
      Skipping adding tag repository, because IMAGE_NAME variable is undefined,
      or it is unsupported.

  when:
    - IMAGE_NAME is undefined
    - 'IMAGE_NAME|lower|regex_search("^auto-")'
